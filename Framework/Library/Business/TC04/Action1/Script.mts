﻿Call FW_TransactionStart ("TC04_Demostore_001_OpenWeb")
Call FW_OpenWebBrowser ("https://demostore.x-cart.com/","CHROME")
Call FW_TransactionEnd ("TC04_Demostore_001_OpenWeb")

Call FW_TransactionStart ("TC04_Demostore_002_Exportdata")
Email = datatable.Value("Email","TC04 [TC04]")
Password = datatable.Value("Password","TC04 [TC04]")
Notes = datatable.Value("Notes","TC04 [TC04]")
Call FW_TransactionEnd ("TC04_Demostore_002_Exportdata")

Call FW_TransactionStart ("TC04_Demostore_003_ClickMyAccount") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_WebElement("TC04","X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up")
Call FW_TransactionEnd ("TC04_Demostore_003_ClickMyAccount")

Call FW_TransactionStart ("TC04_Demostore_004_InputEamil") @@ script infofile_;_ZIP::ssf1.xml_;_
Call FW_WebEdit ("TC04","X-Cart Demo store company","X-Cart Demo store company","login",Email)
Call FW_TransactionEnd ("TC04_Demostore_004_InputEamil")

Call FW_TransactionStart ("TC04_Demoexex_005_InputPassword")
Call FW_WebEdit ("TC04","X-Cart Demo store company","X-Cart Demo store company","password",Password)
Call FW_TransactionEnd ("TC04_Demoexex_005_InputPassword")

Call FW_TransactionStart ("TC04_Demoexex_006_SubmitLogin") @@ script infofile_;_ZIP::ssf3.xml_;_
Call FW_WebButton ("TC04","X-Cart Demo store company","X-Cart Demo store company","Sign in")
Call FW_TransactionEnd ("TC04_Demoexex_006_SubmitLogin")

Call FW_TransactionStart ("TC04_Demoexex_007_ClickNew")
Call FW_Link("TC04","X-Cart Demo store company","X-Cart Demo store company","New!") @@ script infofile_;_ZIP::ssf4.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_007_ClickNew")

Call FW_TransactionStart ("TC04_Demoexex_008_ClickMobilephones")
Call FW_Link("TC04","X-Cart Demo store company","X-Cart Demo store company","Mobile phones") @@ script infofile_;_ZIP::ssf5.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_008_ClickMobilephones")

Call FW_TransactionStart ("TC04_Demoexex_009_ClickiPhone")
Call FW_Image("TC04","X-Cart Demo store company","X-Cart Demo store company","Apple iPhone X [Options")
Call FW_TransactionEnd ("TC04_Demoexex_009_ClickiPhone")

'Call FW_TransactionStart ("TC04_Demoexex_009_SelectColorGold")
'Call  FW_List("TC04","X-Cart Demo store company","X-Cart Demo store company","attribute_values[73]","Gold")
'Call FW_TransactionEnd ("TC04_Demoexex_009_SelectColorGold")

Call FW_TransactionStart ("TC04_Demoexex_010_SelectGB")
Call  FW_List("TC04","X-Cart Demo store company","X-Cart Demo store company","attribute_values[71]","256 (+$200.00)")
Call FW_TransactionEnd ("TC04_Demoexex_010_SelectGB")

Call FW_TransactionStart ("TC04_Demoexex_011_ClickAddtocart")
Call FW_WebButton ("TC04","X-Cart Demo store company","X-Cart Demo store company","Add to cart") @@ script infofile_;_ZIP::ssf9.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_011_ClickAddtocart")

Call FW_TransactionStart ("TC04_Demoexex_012_ClickViewcart")
Call FW_Link("TC04","X-Cart Demo store company","X-Cart Demo store company","View cart") @@ script infofile_;_ZIP::ssf10.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_012_ClickViewcart")

Call FW_TransactionStart ("TC04_Demoexex_013_ClickGotocheckout")
Call FW_WebButton ("TC04","X-Cart Demo store company","X-Cart Demo store company","Go to checkout") @@ script infofile_;_ZIP::ssf11.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_013_ClickGotocheckout")

Call FW_TransactionStart ("TC04_Demoexex_014_Inputnotes")
Call FW_WebEdit ("TC04","X-Cart Demo store company","X-Cart Demo store company","notes",Notes) @@ script infofile_;_ZIP::ssf12.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_014_Inputnotes")

Call FW_TransactionStart ("TC04_Demoexex_015_ClickProceedtopayment")
Call FW_WebButton ("TC04","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment") @@ script infofile_;_ZIP::ssf13.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_015_ClickProceedtopayment")

Call FW_TransactionStart ("TC04_Demoexex_016_ClickPlaceorder")
Call FW_WebButton ("TC04","X-Cart Demo store company","X-Cart Demo store company","Place order") @@ script infofile_;_ZIP::ssf14.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_016_ClickPlaceorder")

Call FW_TransactionStart ("TC04_Demoexex_017_Clickpage-title")
Call FW_WebElement("TC04","X-Cart Demo store company","X-Cart Demo store company","page-title") @@ script infofile_;_ZIP::ssf15.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_017_Clickpage-title")

Call FW_TransactionStart ("TC04_Demoexex_018_ClicMyaccount")
Call FW_WebElement("TC04","X-Cart Demo store company","X-Cart Demo store company","My account") @@ script infofile_;_ZIP::ssf16.xml_;_
Call FW_TransactionEnd ("TC04_Demoexex_018_ClicMyaccount")

Call FW_TransactionStart ("TC04_Demoexex_019_ClicLogout")
Call FW_Link("TC04","X-Cart Demo store company","X-Cart Demo store company","Log out")
Call FW_TransactionEnd ("TC04_Demoexex_019_ClicLogout")

'Call FW_TransactionStart ("TC04_Demoexex_020_CloseWeb")
'Call FW_CloseCurrentBrowser()
'Call FW_TransactionEnd ("TC04_Demoexex_020_CloseWeb")

 @@ script infofile_;_ZIP::ssf20.xml_;_
 @@ script infofile_;_ZIP::ssf21.xml_;_
